// 
// 
// 

#include "GraphPlotter.h"

void GraphPlotter::SetName(char name[NAME_LENGTH]){
	for (int i = 0; i < NAME_LENGTH; i++){
		_name[i] = name[i];
	}
}

void GraphPlotter::Plot(GraphData graphData){
	SerialUSB.print("TimeDebugger ");
	SerialUSB.print(_name);
	SerialUSB.println(":");
	for (int i = 0; i < RANGE_SIZE; i++){
		unsigned char appeareancePercentage = (100*graphData.timesAppeared[i])/graphData.totalData;
		for (int j = 0; j < 100; j++){
			(j<=appeareancePercentage) ? SerialUSB.print("|") : SerialUSB.print(" ");
		}
		SerialUSB.print(" ");
		SerialUSB.print(i == 0 ? graphData.minTime : graphData.ranges[i-1]);
		SerialUSB.print("-");
		SerialUSB.print(graphData.ranges[i]);
		SerialUSB.print(": ");
		SerialUSB.println(graphData.timesAppeared[i]);
	}
	SerialUSB.println("----------------------------------");
	
}


