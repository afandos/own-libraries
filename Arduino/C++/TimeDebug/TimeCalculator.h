// TimeCalculator.h
#include "TimeStatistics.h"


#ifndef _TIMECALCULATOR_h
#define _TIMECALCULATOR_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

class TimeCalculator
{
	 long _functionStartTime;
	unsigned short _executionCount;
	 short _averageExecutionTime;
	 long _maxExecutionTime = 0;
	 long _minExecutionTime = 65000;
	 long _functionTotalExecutionTime;
	 long _functionConsumedTime;
	 
	void CalculateElapsedTime();
	void IncreaseExecutionCount();
	void CalculateAverageExecutionTime();
	void CalculateMaxExecutionTime();
	void CalculateMinExecutionTime();
	TimeStatistics GetStatistics();
	
	public:
	void Reset();
	void Start();
	TimeStatistics StopAndGetTimeStatistics();
	
};

#endif

