// TimeStatistics.h

#ifndef _TIMESTATISTICS_h
#define _TIMESTATISTICS_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif



struct TimeStatistics{
		long printFrequency;
		unsigned short executionCount;
		 short averageExecutionTime;
		 long maxExecutionTime;
		 long minExecutionTime;
		 long  totalExeceutionTime;
		 long lastFunctionTime;
	};

#endif

