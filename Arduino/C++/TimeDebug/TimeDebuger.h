// TimeDebuger.h
#include "TimePrinter.h"
#include "TimeCalculator.h"
#include "TimeStatistics.h"
#include "GraphData.h"
#include "GraphCalculator.h"
#include "GraphPlotter.h"

#ifndef _TIMEDEBUGER_h
#define _TIMEDEBUGER_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#define NAME_LENGTH 20

#define TEXT 0
#define GRAPH 1

class TimeDebuger
{
	bool _cycleStarted;
	unsigned char _name[NAME_LENGTH];
	unsigned char _mode;
	unsigned long _printPeriod = 1000000;
	unsigned long _cycleStartTime;
	unsigned long _executionStartTime;
	TimePrinter _timePrinter;
	TimeCalculator _timeCalculator;
	GraphCalculator _graphCalculator;
	GraphPlotter _graphPlotter;
	unsigned char numberOfActivations = 0;
	void RegisterCycleStartTime();
	bool hasCycleFinished();

	public:
	TimeDebuger(char name[NAME_LENGTH], unsigned long printFrequency, unsigned char mode);
	void Start();
	void Stop();
	
};

#endif

