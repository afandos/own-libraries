// GraphCalculator.h

#include "GraphData.h"

#ifndef _GRAPHCALCULATOR_h
#define _GRAPHCALCULATOR_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif


class GraphCalculator
{
	
	long _maxTime = 0;
	long _minTime = 1000000;
	float _ranges[RANGE_SIZE];
	unsigned long _timesAppeared[RANGE_SIZE];
	unsigned long _totalData = 0;
	bool CalculateNewBounds(unsigned long elapsedTime);
	void CalculateNewRanges();
	public:
	void StoreNewData(unsigned long elapsedTime);
	GraphData GetGraphData();
	void Reset();
	
};

#endif

