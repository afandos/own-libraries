// 
// 
// 

#include "TimeDebuger.h"

TimeDebuger::TimeDebuger(char name[NAME_LENGTH], unsigned long printFrequency, unsigned char mode)
{
	_mode = mode;
	_timePrinter.SetName(name);
	_graphPlotter.SetName(name);
	_printPeriod = printFrequency;
}

void TimeDebuger::Start()
{
	_timeCalculator.Start();
	RegisterCycleStartTime();
}

void TimeDebuger::RegisterCycleStartTime(){
	if (_cycleStarted == false) {
		_cycleStarted = true;
		_cycleStartTime = micros();
	}
}

void TimeDebuger::Stop()
{

	TimeStatistics timeStatistics = _timeCalculator.StopAndGetTimeStatistics();
	timeStatistics.printFrequency = _printPeriod;
	_graphCalculator.StoreNewData(timeStatistics.lastFunctionTime);
	if(hasCycleFinished()){
		if (_mode == TEXT) _timePrinter.Print(timeStatistics);
		_timeCalculator.Reset();
		
		GraphData graphData = _graphCalculator.GetGraphData();
		_graphCalculator.Reset();
		if (_mode == GRAPH) _graphPlotter.Plot(graphData);
	}
}

bool TimeDebuger::hasCycleFinished()
{
	if (micros() - _cycleStartTime > _printPeriod){
		_cycleStarted = false;
		return true;
	}
	return false;
}


