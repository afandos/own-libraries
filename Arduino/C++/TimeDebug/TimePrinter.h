// TimePrinter.h

#include "TimeStatistics.h"

#define NAME_LENGTH 200

#ifndef _TIMEPRINTER_h
#define _TIMEPRINTER_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

class TimePrinter
{
	char _name[NAME_LENGTH];
	unsigned long _timeStamp;

	public:
	void SetName(char name[NAME_LENGTH]);
	void Print(TimeStatistics timeStatistics);
};

#endif

