// GraphData.h

#ifndef _GRAPHDATA_h
#define _GRAPHDATA_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#define RANGE_SIZE 10

struct GraphData{
	long minTime;
	float ranges[RANGE_SIZE];
	unsigned long timesAppeared[RANGE_SIZE];
	unsigned long totalData;
};

#endif

