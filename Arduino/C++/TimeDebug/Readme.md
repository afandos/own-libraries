# Time Debug

This library calculates the average processing time of the programme's choice of code part.
The information is displayed either in numerical values or through a graph.