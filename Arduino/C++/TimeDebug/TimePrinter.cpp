// 
// 
// 

#include "TimePrinter.h"
void TimePrinter::SetName(char name[NAME_LENGTH]){
	for (int i = 0; i < NAME_LENGTH; i++){
		_name[i] = name[i];
	}
}


void TimePrinter::Print(TimeStatistics timeStatistics){
	SerialUSB.print("TimeDebugger ");
	SerialUSB.print(_name);
	SerialUSB.print(" has been executed ");
	SerialUSB.print(timeStatistics.executionCount);
	SerialUSB.print(" times in ");
	SerialUSB.print(timeStatistics.printFrequency);
	SerialUSB.println(" us. ");
	SerialUSB.print("The average execution time is ");
	SerialUSB.print(timeStatistics.averageExecutionTime);
	SerialUSB.println(" us.");
	SerialUSB.print("The maximum execution time is ");
	SerialUSB.print(timeStatistics.maxExecutionTime);
	SerialUSB.println(" us.");
	SerialUSB.print("The minimum execution time is ");
	SerialUSB.print(timeStatistics.minExecutionTime);
	SerialUSB.println(" us.");
	SerialUSB.print("Total execution time was ");
	SerialUSB.print(timeStatistics.totalExeceutionTime);
	SerialUSB.println("us.");
	SerialUSB.print("It consumes ");
	SerialUSB.print((float)(timeStatistics.totalExeceutionTime) / (float)timeStatistics.printFrequency*100.0);
	SerialUSB.println("% of the processing time.");
	SerialUSB.println("----------------------------------");
}



