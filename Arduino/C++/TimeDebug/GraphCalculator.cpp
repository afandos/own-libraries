// 
// 
// 

#include "GraphCalculator.h"





void GraphCalculator::StoreNewData(unsigned long elapsedTime){
	_totalData++;
	if (CalculateNewBounds(elapsedTime)){
		CalculateNewRanges();
	}
	for (int i = 0; i < RANGE_SIZE; i++){
		if (elapsedTime < _ranges[i]){
			_timesAppeared[i]++;
			break;
		}
	}
}

void GraphCalculator::Reset()
{
	for (int i = 0; i < RANGE_SIZE; i++){
		_timesAppeared[i] = 0;
	}
	_totalData = 0;
}

GraphData GraphCalculator::GetGraphData(){
	GraphData graphData;
	graphData.minTime = _minTime;
	graphData.totalData = _totalData;
	for (int i = 0; i < RANGE_SIZE; i++){
		graphData.ranges[i] = _ranges[i];
		graphData.timesAppeared[i] = _timesAppeared[i];
	}
	return graphData;
}

void GraphCalculator::CalculateNewRanges(){
	float totalRange = _maxTime - _minTime;
	for (int i = 0; i < RANGE_SIZE; i++){
		_ranges[i] = (float)_minTime+totalRange/(float)RANGE_SIZE*(float)(i+1);
	}
	
}

bool GraphCalculator::CalculateNewBounds(unsigned long elapsedTime){
	bool changed = false;
	if (_maxTime < elapsedTime) {
		_maxTime = elapsedTime;
		changed = true;
	}
	if (_minTime > elapsedTime) 
	{
		_minTime = elapsedTime;
		changed = true;
	}
	return changed;
}
