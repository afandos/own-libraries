// GraphPlotter.h

#include "GraphData.h"

#ifndef _GRAPHPLOTTER_h
#define _GRAPHPLOTTER_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#define NAME_LENGTH 200

class GraphPlotter{
	char _name[NAME_LENGTH];
	
	public:
	void SetName(char name[NAME_LENGTH]);
	void Plot(GraphData graphData);
};


#endif

