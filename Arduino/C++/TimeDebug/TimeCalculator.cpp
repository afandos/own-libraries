// 
// 
// 

#include "TimeCalculator.h"


void TimeCalculator::Reset(){
	_executionCount = 0;
	_averageExecutionTime = 0;
	_maxExecutionTime = 0;
	_minExecutionTime = 1000000;
	_functionTotalExecutionTime = 0;
	_functionConsumedTime = 0;

}
void TimeCalculator::Start(){
	_functionStartTime = micros();
}
TimeStatistics TimeCalculator::StopAndGetTimeStatistics(){
	IncreaseExecutionCount();
	CalculateElapsedTime();
	CalculateAverageExecutionTime();
	CalculateMaxExecutionTime();
	CalculateMinExecutionTime();
	_functionStartTime = micros();
	
	
	return GetStatistics();
}

void TimeCalculator::CalculateElapsedTime(){
	_functionConsumedTime = micros() - _functionStartTime;
	if (_functionConsumedTime < 0){
		Reset();
	}
	_functionTotalExecutionTime = _functionTotalExecutionTime + _functionConsumedTime;
}
void TimeCalculator::IncreaseExecutionCount(){
	_executionCount++;
}
void TimeCalculator::CalculateAverageExecutionTime(){
	_averageExecutionTime = _functionTotalExecutionTime / _executionCount;
}
void TimeCalculator::CalculateMaxExecutionTime(){
	if (_functionConsumedTime > _maxExecutionTime) _maxExecutionTime = _functionConsumedTime;
}
void TimeCalculator::CalculateMinExecutionTime(){
	if (_functionConsumedTime < _minExecutionTime) _minExecutionTime = _functionConsumedTime;
}

TimeStatistics TimeCalculator::GetStatistics(){
	TimeStatistics timeStatistics;
	timeStatistics.averageExecutionTime = _averageExecutionTime;
	timeStatistics.executionCount = _executionCount;
	timeStatistics.maxExecutionTime = _maxExecutionTime;
	timeStatistics.minExecutionTime = _minExecutionTime;
	timeStatistics.totalExeceutionTime = _functionTotalExecutionTime;
	timeStatistics.lastFunctionTime = _functionConsumedTime;
	return timeStatistics;
}
