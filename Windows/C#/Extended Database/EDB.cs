﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows;
using System.Reflection;

namespace ExtendedDatabase
{
    class EDB
    {
        private const uint _headerSize = 16;
        private uint _numberOfRecords;
        private uint _tableSize;
        private uint _recordSize;
        private uint _codificationID;
        private object _database;
        private byte[] _bytesToBeWritten;
        private FileStream file;
        string _path;
        IOrderedEnumerable<FieldInfo> fieldInfo;
   


        public EDB(object database, uint tableSize, string path)
        {
            
            _path = path;
            _database = database;
            //fieldInfo = GetFieldsInfo(_database);
            fieldInfo = (_database.GetType().GetFields().OrderBy(f => f.MetadataToken));
  
                _tableSize = tableSize;
            if (File.Exists(_path))
            {
                GetInfoFromFile();
            }
            else
            {
                GetHeaderInfoFromClass();
                _bytesToBeWritten = new byte[_tableSize];
                _codificationID = (uint)_database.GetType().GetField("ID").GetValue(_database);
                AppendEntry();
            }
           
        }

        public void SaveData()
        {
            OpenFile();
            WriteFile();
            CloseFile();
        }        

        public void AppendEntry()
        {
            try
            {
                CheckCodificationID();
                WriteEntry(_numberOfRecords);
                IncreaseNumberOfEntries();
                WriteHeader();
            }
            catch (System.InvalidOperationException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void ModifyEntry(uint entryNumber)
        {
            try
            {
                CheckCodificationID();
                WriteEntry(entryNumber);
                WriteHeader();
            }
            catch (System.InvalidOperationException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void GetEntry(uint entryNumber)
        {
            try
            {
                ReadEntry(entryNumber);
            }
            catch (System.InvalidOperationException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void OpenFile()
        {
            file = new FileStream(_path, FileMode.OpenOrCreate, FileAccess.ReadWrite);
        }

        private void GetInfoFromFile()
        {
            OpenFile();
            ReadHeader();
            ReadCodificationID();
            ExtractDataArray();
            CloseFile();
        }

        private void ReadHeader()
        {
            byte[] header = new byte[16];
            file.Read(header, 0, 16);
            if (IsHeaderValid(header))
            {
                byte[] decomposedNumberOfRecords = new byte[4];
                byte[] decomposedRecordSize = new byte[4];
                byte[] decomposedTableSize = new byte[4];

                Array.Copy(header, 4, decomposedNumberOfRecords, 0, 4);
                Array.Copy(header, 8, decomposedRecordSize, 0, 4);
                Array.Copy(header, 12, decomposedTableSize, 0, 4);

                _numberOfRecords = BitConverter.ToUInt32(decomposedNumberOfRecords, 0);
                _recordSize = BitConverter.ToUInt32(decomposedRecordSize, 0);
                _tableSize = BitConverter.ToUInt32(decomposedTableSize, 0);
            }

            else
            {
                MessageBox.Show("Database is not valid format");
            }
        }

        private void ReadCodificationID()
        {
            byte[] codificationID = new byte[4];
            file.Position = 16;
            file.Read(codificationID, 0, 4);
            _codificationID = BitConverter.ToUInt32(codificationID, 0);
        }

        private void ExtractDataArray()
        {
            file.Position = 0;
            _bytesToBeWritten = new byte[_tableSize];
            file.Read(_bytesToBeWritten, 0, (int)file.Length);
        }

        private void CloseFile()
        {
            file.Close();
        }

        private void GetHeaderInfoFromClass()
        {
            _recordSize = (uint)(fieldInfo.Count<FieldInfo>() * 4);
            _numberOfRecords = 0;
        }

        

        private FieldInfo[] GetFieldsInfo(object obj)
        {
            return obj.GetType().GetFields();
        }

        

        private void WriteHeader()
        {
            byte[] decomposedNumberOfRecords = BitConverter.GetBytes(_numberOfRecords);
            byte[] decomposedRecordSize = BitConverter.GetBytes(_recordSize);
            byte[] decomposedTableSize = BitConverter.GetBytes(_tableSize);

            _bytesToBeWritten[0] = 0xDB;
            for(byte i = 0; i < 4; i++) _bytesToBeWritten[4 + i] = decomposedNumberOfRecords[i];
            for(byte i = 0; i < 4; i++) _bytesToBeWritten[8 + i] = decomposedRecordSize[i];
            for(byte i = 0; i < 4; i++) _bytesToBeWritten[12 + i] = decomposedTableSize[i];
        }

        

        private bool IsHeaderValid(byte[] header)
        {
            return header[0] == 0xdb;
        }

        private void CheckCodificationID()
        {
            if (_database.GetType().GetField("ID") == null)
                throw new System.InvalidOperationException("Database has no 'ID' field");
            if ((uint)_database.GetType().GetField("ID").GetValue(_database) != _codificationID)
                throw new System.InvalidOperationException("Codification ID mismatch error");
        }

        private void WriteEntry(uint entryNumber)
        {
            if (entryNumber > _numberOfRecords)
            {
                throw new System.ArgumentException("Requested entry is out of range");
            }
            for (int i = 0; i < fieldInfo.Count<FieldInfo>(); i++)
            {
                Type type = fieldInfo.ElementAt<FieldInfo>(i).FieldType;
                byte[] bytes = new byte[] { };
                switch (type.Name.ToLower())
                {
                    case "uint32":
                        bytes = BitConverter.GetBytes((uint)fieldInfo.ElementAt<FieldInfo>(i).GetValue(_database));
                        break;
                    case "int32":
                        bytes = BitConverter.GetBytes((int)fieldInfo.ElementAt<FieldInfo>(i).GetValue(_database));
                        break;
                    case "single":
                        bytes = BitConverter.GetBytes((float)fieldInfo.ElementAt<FieldInfo>(i).GetValue(_database));
                        break;
                    case "bool":
                    case "boolean":
                        bytes = BitConverter.GetBytes((bool)fieldInfo.ElementAt<FieldInfo>(i).GetValue(_database));
                        break;
                    case "byte":
                        bytes = BitConverter.GetBytes((byte)fieldInfo.ElementAt<FieldInfo>(i).GetValue(_database));
                        break;
                    default:
                        break;
                }
                for (byte j = 0; j < bytes.Length; j++) _bytesToBeWritten[_headerSize + (entryNumber) * _recordSize + i * 4 + j] = bytes[j];
            }
        }

        private void ReadEntry(uint entryNumber)
        {
            if (entryNumber >= _numberOfRecords)
            {
                throw new System.ArgumentException("Requested entry is out of range");
            }
            for (int i = 0; i < fieldInfo.Count<FieldInfo>(); i++)
            {
                if (fieldInfo.ElementAt<FieldInfo>(i).Name != "ID")
                {
                    Type type = fieldInfo.ElementAt<FieldInfo>(i).FieldType;
                    byte[] bytes = new byte[4];
                    for (byte j = 0; j < bytes.Length; j++) bytes[j] = _bytesToBeWritten[_headerSize + (entryNumber) * _recordSize + i * 4 + j];

                    switch (type.Name.ToLower())
                    {
                        case "uint32":
                            fieldInfo.ElementAt<FieldInfo>(i).SetValue(_database, BitConverter.ToUInt32(bytes, 0));
                            break;
                        case "int32":
                            fieldInfo.ElementAt<FieldInfo>(i).SetValue(_database, BitConverter.ToInt32(bytes, 0));
                            break;
                        case "single":
                            fieldInfo.ElementAt<FieldInfo>(i).SetValue(_database, BitConverter.ToSingle(bytes, 0));
                            break;
                        case "bool":
                        case "boolean":
                            fieldInfo.ElementAt<FieldInfo>(i).SetValue(_database, BitConverter.ToBoolean(bytes, 0));
                            break;
                        case "byte":
                            fieldInfo.ElementAt<FieldInfo>(i).SetValue(_database, BitConverter.ToBoolean(bytes, 0));
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        private void IncreaseNumberOfEntries()
        {
            _numberOfRecords++;
        }

        private void WriteFile()
        {
            file.Write(_bytesToBeWritten, 0, (int)(_headerSize + _numberOfRecords * _recordSize));
        }
    }
}
